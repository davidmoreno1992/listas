package com.example.lista

import android.text.Layout
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

class ListRecyclerViewAdapter( private val lists: ArrayList<TaskList> = ArrayList())
    :RecyclerView.Adapter<ListViewHolder>() {
    val mainList= arrayOf("Shopping List","Homework","Chores")

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListViewHolder {
        val view= LayoutInflater.from(parent.context)
            .inflate(R.layout.view_holder,parent,false)
        return ListViewHolder(view)

    }

    override fun getItemCount(): Int {
        return lists.size
    }

    fun addList(list: TaskList){
        lists.add(list)
        notifyItemInserted(lists.size-1)

    }

    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {
        holder.listItemId.text=(position+1).toString()
        holder.listtItemTitle.text=lists[position].name
    }
}